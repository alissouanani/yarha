<!DOCTYPE html>
<html lang="zxx">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/line-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/swiper-min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/odometre.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dark-theme.css') }}">
    <title>Yarha</title>
    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.png') }}">
</head>

<body>

    <div class="preloader js-preloader">
        <img src="{{ asset('assets/img/preloader.gif') }}" alt="Image">
    </div>


    <div class="switch-theme-mode">
        <label id="switch" class="switch">
            <input type="checkbox" onchange="toggleTheme()" id="slider">
            <span class="slider round"></span>
        </label>
    </div>


    <div class="page-wrapper ">

        <header class="header-wrap style1">
            <div class="header-top">
                <div class="container">
                    <div class="close-header-top xl-none">
                        <button type="button"><i class="las la-times"></i></button>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-xl-10 col-lg-12">
                            <div class="header-top-left">
                                <div class="contact-item">
                                    <i class="flaticon-wall-clock"></i>
                                    <p>Mon - Sun[8:00am - 10:00pm]</p>
                                </div>
                                <div class="contact-item">
                                    <i class="flaticon-phone-call"></i>
                                    <a href="tel:13454567877">800-323-4567</a>
                                </div>
                                <div class="contact-item">
                                    <i class="flaticon-voucher"></i>
                                    <p>Discount 0ff <strong>25%</strong>only for <span>Burger Item</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-12">
                            <div class="header-top-right">
                                <div class="select-lang">
                                    <div class="navbar-option-item navbar-language dropdown language-option">
                                        <button class="dropdown-toggle" type="button" data-bs-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            <span class="lang-name"></span>
                                        </button>
                                        <div class="dropdown-menu language-dropdown-menu">
                                            <a class="dropdown-item" href="">
                                                <img src="assets/img/uk.png" alt="flag">
                                                English
                                            </a>
                                            <a class="dropdown-item" href="">
                                                <img src="assets/img/china.png" alt="flag">
                                                简体中文
                                            </a>
                                            <a class="dropdown-item" href="">
                                                <img src="{{ ('assets/img/uae.png') }}" alt="flag">
                                                العربيّة
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="select-currency">
                                    <select>
                                        <option value="1">USD</option>
                                        <option value="2">GBP</option>
                                        <option value="3">YEN</option>
                                    </select>
                                </div>
                            </div>
                            <a href="" class="btn style1 xl-none">Docummentation<i
                                    class="flaticon-right-arrow-2"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container ">
                <div class="header-bottom">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-6 col-md-6 col-5">
                            <a href="" class="logo"><img src="{{ asset('assets/img/logo-white.png') }}" alt="Image"></a>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-7">
                            <div class="main-menu-wrap">
                                <div class="menu-close xl-none">
                                    <a href="javascript:void(0)"><i class="las la-times"></i></a>
                                </div>
                            </div>
                            <div class="mobile-bar-wrap">
                                <div class="mobile-top-bar xl-none">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="mobile-menu xl-none">
                                    <a href="javascript:void(0)"><i class="las la-bars"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 lg-none">
                            <div class="header-bottom-right">
                                <a href="" class="btn style1">Documentation<i
                                        class="flaticon-right-arrow-2"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <section class="hero-wrap style1 bg-cod-grey">
            <img src="assets/img/hero/hero-shape-1.png" alt="Image" class="hero-shape-1">
            <img src="assets/img/hero/hero-shape-2.png" alt="Image" class="hero-shape-2">
            <img src="assets/img/hero/hero-shape-31.png" alt="Image" class="hero-shape-3 md-none">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="hero-content">
                            <h1>We Provide Express Home Delivery</h1>
                            <p>In shakes item of 2021 we are offering
                                20% flat discount. Don't miss out!!</p>
                            <div class="hero-btn">
                                <a href="" class="btn style1"><i
                                        class="las la-shopping-bag"></i>Order Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="hero-img-wrap">
                            <img src="{{ asset('assets/img/hero/hero-img-1.png') }}" alt="Image">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/form-validator.min.js') }}"></script>
    <script src="{{ asset('assets/js/contact-form-script.js') }}"></script>
    <script src="{{ asset('assets/js/aos.js') }}"></script>
    <script src="{{ asset('assets/js/swiper-min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-magnific-popup.js') }}"></script>
    <script src="{{ asset('assets/js/fancybox.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.appear.js') }}"></script>
    <script src="{{ asset('assets/js/odometre.min.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
</body>

</html>
