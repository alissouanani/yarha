<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('prix');
            $table->mediumText('description');
            $table->json('images')->nullable();
            $table->json('infos_supplementaire')->nullable();
            $table->string('prix_promo')->nullable();
            $table->string('note_produit')->nullable();
            $table->foreignId('marchand_id')->constrained('marchands')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('categorie_produit_id')->constrained('categorie_produits')->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('produits');
    }
};
