<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('notable_id');
            $table->string('notable_type');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('produit_id')->nullable();
            $table->unsignedBigInteger('marchand_id')->nullable();
            $table->unsignedBigInteger('livreur_id')->nullable();
            $table->unsignedTinyInteger('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notes');
    }
};
