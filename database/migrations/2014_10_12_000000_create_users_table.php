<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('no_ifu')->nullable();
            $table->string('nom');
            $table->string('prenoms');
            $table->string('tel')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('sexe')->nullable();
            $table->json('piece_utilisateur')->nullable();
            $table->string('solde')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('note')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('isDelete')->default(0);
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
