<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('marchands', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('type_marchand_id')->constrained('type_marchands')->onUpdate('cascade')->onDelete('cascade');
            $table->string('no_ifu');
            $table->string('nom');
            $table->string('adresse');
            $table->string('contact');
            $table->string('email')->unique();
            $table->string('note_marchand')->nullable();
            $table->json('geo_localisation')->nullable();
            $table->boolean('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('marchands');
    }
};
