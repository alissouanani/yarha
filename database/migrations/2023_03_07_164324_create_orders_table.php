<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('marchand_id')->constrained('marchands')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('type_course_id')->constrained('type_courses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('livreur_id')->nullable()->constrained('livreurs')->onUpdate('cascade')->onDelete('cascade');
            $table->string('reference')->nullable();
            $table->string('frais_course');
            $table->string('total');
            $table->enum('status', ['en attente', 'En cours', 'livrer'])->default('en attente');
            $table->string('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
