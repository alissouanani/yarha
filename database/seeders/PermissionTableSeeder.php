<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Permission::create(['name' => 'edit user']);
        Permission::create(['name' => 'Add user']);
        Permission::create(['name' => 'Delete user']);

        Permission::create(['name' => 'edit message']);
        Permission::create(['name' => 'Add message']);
        Permission::create(['name' => 'Delete message']);

        Permission::create(['name' => 'edit note']);
        Permission::create(['name' => 'Add note']);
        Permission::create(['name' => 'Delete note']);

        Permission::create(['name' => 'edit course']);
        Permission::create(['name' => 'Add course']);
        Permission::create(['name' => 'Delete course']);

        Permission::create(['name' => 'edit marchand']);
        Permission::create(['name' => 'Add marchand']);
        Permission::create(['name' => 'Delete marchand']);

        Permission::create(['name' => 'edit livreur']);
        Permission::create(['name' => 'Add livreur']);
        Permission::create(['name' => 'Delete livreur']);

        Permission::create(['name' => 'edit rôle']);
        Permission::create(['name' => 'Add rôle']);
        Permission::create(['name' => 'Delete rôle']);

        Permission::create(['name' => 'edit room']);
        Permission::create(['name' => 'Add room']);
        Permission::create(['name' => 'Delete room']);

        Permission::create(['name' => 'edit permission']);
        Permission::create(['name' => 'Add permission']);
        Permission::create(['name' => 'Delete permission']);

        Permission::create(['name' => 'edit Produit']);
        Permission::create(['name' => 'Add Produit']);
        Permission::create(['name' => 'Delete Produit']);
    }
}
