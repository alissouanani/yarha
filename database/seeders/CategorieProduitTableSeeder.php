<?php

namespace Database\Seeders;

use App\Models\CategorieProduit;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorieProduitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CategorieProduit::create(['nom' => 'Nourriture']);
        CategorieProduit::create(['nom' => 'vêtements']);
        CategorieProduit::create(['nom' => 'produit cosmétique']);
        CategorieProduit::create(['nom' => 'mobilier']);
    }
}
