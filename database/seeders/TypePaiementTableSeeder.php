<?php

namespace Database\Seeders;

use App\Models\Paiement;
use App\Models\TypePaiement;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TypePaiementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TypePaiement::create(['type' => 'Ligne']);
        TypePaiement::create(['type' => 'Espèce']);
    }
}
