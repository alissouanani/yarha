<?php

namespace Database\Seeders;

use App\Models\Room;
use App\Models\Message;
use App\Models\Produit;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();
        for ($i = 0; $i < 20; $i++) {
            Message::create([
                'message' => $faker->sentence(),
                'room_id' => Room::all()->random()->id,
            ]);
        }
    }
}
