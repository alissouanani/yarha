<?php

namespace Database\Seeders;

use App\Models\TypeCourse;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TypeCourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TypeCourse::create(['type' => 'Ordinaire']);
        TypeCourse::create(['type' => 'course repas']);
    }
}
