<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([

            // Administration
            [
                'id' =>1,
                'nom' => 'Admin',
                'prenoms' => 'Admin',
                'email' => 'admin@admin.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],

            // utilisateurs

            [
                'id' =>2,
                'nom' => 'LOKO',
                'prenoms' => 'aristofane smith',
                'email' => 'aristofanesmithloko@gmail.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],
            [
                'id' =>3,
                'nom' => 'DOUMEFIO',
                'prenoms' => 'Anne',
                'email' => 'doumefio@gmail.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],
            [
                'id' =>4,
                'nom' => 'ALISSOU',
                'prenoms' => 'Fernando',
                'email' => 'alissouanani@gmail.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password_default' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],

            // livreur
            [
                'id' =>5,
                'nom' => 'LOKO livreur',
                'prenoms' => 'aristofane smith livreur',
                'email' => 'aristofanesmithlokolivreur@gmail.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],
            [
                'id' =>6,
                'nom' => 'DOUMEFIO livreur',
                'prenoms' => 'Anne livreur',
                'email' => 'doumefiolivreur@gmail.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],
            [
                'id' =>7,
                'nom' => 'ALISSOU livreur',
                'prenoms' => 'Fernando livreur',
                'email' => 'alissouananilivreur@gmail.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password_default' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],

            // marchand
            [
                'id' =>8,
                'nom' => 'LOKO marchand',
                'prenoms' => 'aristofane smith marchand',
                'email' => 'aristofanesmithlokomarchand@gmail.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],
            [
                'id' =>9,
                'nom' => 'DOUMEFIO marchand',
                'prenoms' => 'Anne marchand',
                'email' => 'doumefiomarchand@gmail.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],
            [
                'id' =>10,
                'nom' => 'ALISSOU marchand',
                'prenoms' => 'Fernando marchand',
                'email' => 'alissouananimarchand@gmail.com',
                'tel' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'password_default' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'isDelete' => 0,
            ],


        ]);


        DB::table('model_has_roles')->insert([

            [
                'role_id' => 2,
                'model_type' => 'App\Models\User',
                'model_id' => 1,
            ],

            // utilisateurs

            [
                'role_id' => 1,
                'model_type' => 'App\Models\User',
                'model_id' => 2,
            ],
            [
                'role_id' => 1,
                'model_type' => 'App\Models\User',
                'model_id' => 3,
            ],
            [
                'role_id' => 1,
                'model_type' => 'App\Models\User',
                'model_id' => 4,
            ],

            // Livreur
            [
                'role_id' => 4,
                'model_type' => 'App\Models\User',
                'model_id' => 5,
            ],
            [
                'role_id' => 4,
                'model_type' => 'App\Models\User',
                'model_id' => 6,
            ],
            [
                'role_id' => 4,
                'model_type' => 'App\Models\User',
                'model_id' => 7,
            ],

            //  marchand
            [
                'role_id' => 3,
                'model_type' => 'App\Models\User',
                'model_id' => 8,
            ],
            [
                'role_id' => 3,
                'model_type' => 'App\Models\User',
                'model_id' => 9,
            ],
            [
                'role_id' => 3,
                'model_type' => 'App\Models\User',
                'model_id' => 10,
            ],
        ]);


    }
}
