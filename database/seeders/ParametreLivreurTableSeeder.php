<?php

namespace Database\Seeders;

use App\Models\ParametreLivreur;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ParametreLivreurTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ParametreLivreur::create(['commission' => '10']);
        ParametreLivreur::create(['commission' => '5']);
        ParametreLivreur::create(['commission' => '8']);
        ParametreLivreur::create(['commission' => '2']);
    }
}
