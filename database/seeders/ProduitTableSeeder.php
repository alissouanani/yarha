<?php

namespace Database\Seeders;

use App\Models\CategorieProduit;
use App\Models\Marchand;
use App\Models\Produit;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;


class ProduitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();
        $productName = $faker->unique()->words($nb = 4, $asText = true);
        // $Categorys = Category::all()->pluck('id')->toArray();

        for ($i = 0; $i < 20; $i++) {
            Produit::create([
                'nom' => $productName,
                'description' => $faker->sentence(),
                'prix_promo' => mt_rand(80, 180),
                'prix' => mt_rand(100, 150),
                'marchand_id' => Marchand::all()->random()->id,
                'categorie_produit_id' => CategorieProduit::all()->random()->id,
            ]);
        }
    }
}
