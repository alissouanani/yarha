<?php

namespace Database\Seeders;

use App\Models\TypeMarchand;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MarchandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('marchands')->insert([


            [
                'id' =>1,
                'nom' => 'ETS DOUMEFIO',
                'no_ifu' => '6708475847584754870595',
                'email' => 'doumefio@livreur.com',
                'contact' => '67080595',
                'user_id' => 8,
                'type_marchand_id' => TypeMarchand::all()->random()->id,
                'adresse' => 'cotonou',
            ],
            [
                'id' =>2,
                'nom' => ' ETS ALISSOU',
                'email' => 'alissouanani@livreur.com',
                'contact' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'user_id' => 9,
                'type_marchand_id' => TypeMarchand::all()->random()->id,
                'adresse' => 'cotonou',
            ],


            [
                'id' =>3,
                'nom' => 'ETS livreur',
                'email' => 'alissouananilivreur@livreur.com',
                'contact' => '67080595',
                'no_ifu' => '6708475847584754870595',
                'adresse' => 'cotonou',
                'user_id' => 10,
                'type_marchand_id' => TypeMarchand::all()->random()->id,
            ],
        ]);

    }
}
