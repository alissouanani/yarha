<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class LivreurTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('livreurs')->insert([


            [
                'id' =>1,
                'matricule_engin' => '6708475847584754870595',
                'user_id' => 5,
            ],
            [
                'id' =>2,
                'matricule_engin' => '6708475847584754870595',
                'user_id' => 6,
            ],


            [
                'id' =>3,
                'matricule_engin' => '6708475847584754870595',
                'user_id' => 7,
            ],

        ]);

    }
}
