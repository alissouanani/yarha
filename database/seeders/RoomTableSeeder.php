<?php

namespace Database\Seeders;

use App\Models\Room;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        for ($i = 0; $i < 10; $i++){
            Room::create([
                'receiver_id' => User::all()->random()->id,
                'sender_id' => User::all()->random()->id,
            ]);

        }


    }
}
