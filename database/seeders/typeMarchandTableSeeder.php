<?php

namespace Database\Seeders;

use App\Models\TypeMarchand;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class typeMarchandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TypeMarchand::create(['type' => 'boutique']);
        TypeMarchand::create(['type' => 'Resto']);
    }
}
