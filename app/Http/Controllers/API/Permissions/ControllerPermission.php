<?php

namespace App\Http\Controllers\API\Permissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;

class ControllerPermission extends Controller
{
    // Récupérer tous les permisions
    public function AllPermissions()
    {
        $permissions = Permission::where('isDelete', 0)->get();
        return response()->json(['success' => true, 'response' => $permissions],200);
    }

    // Ajouter une permissions

    public function addPermission(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $permission = Permission::create([
            'name' => $request->name,
        ]);

        return response()->json(['success' => true, 'response' => $permission], 201);
    }

    // mettre a jour une permission

    public function updatePermission(Request $request, $permission_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $permission = Permission::where('isDelete', 0)->where('id', $permission_id)->first();

        $permission->name = $request->name;
        $permission->save();

        if($permission)
        {
            return response()->json(['success' => true, 'response' => $permission], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Cette Permission n\'existe pas dans notre systeme'], 404);

        }

    }

    // supprimer une permissions
    public function deletePermission($permission_id){

        $permission = Permission::where('isDelete', 0)->where('id', $permission_id)->first();
        if($permission)
        {
            $permission->isDelete = 1;
            $permission->save();
            return response()->json(['success' => true, 'message' => 'Cette Permission à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'cette Permission n\'est pas le systéme'], 404);

        }
    }

}
