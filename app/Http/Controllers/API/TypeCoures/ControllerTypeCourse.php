<?php

namespace App\Http\Controllers\API\TypeCoures;

use App\Models\TypeCourse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ControllerTypeCourse extends Controller
{
    // Récupérer tous les types de courses
    public function AllTypeCourse()
    {
        $typeCourses = TypeCourse::where('isDelete', 0)->get();

        return response()->json(['success' => true, 'response' => $typeCourses],200);

    }
    // Ajouter un types de courses
    public function addTypeCourse(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required', 'string'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $typeCourse = TypeCourse::create([
            'type' => $request->type,
        ]);

        return response()->json(['success' => true, 'response' => $typeCourse], 201);
    }

    // mettre a jour un types de courses
    public function updateTypeCourse(Request $request, $typeCourse_id)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required', 'string'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $typeCourse = TypeCourse::where('isDelete', 0)->where('id', $typeCourse_id)->first();


        $typeCourse->type = $request->type;
        $typeCourse->save();



        if($typeCourse)
        {
            return response()->json(['success' => true, 'response' => $typeCourse], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Ce type de course n\'existe pas dans notre systeme'], 404);

        }

    }

    // supprimer un types de courses

    public function deleteTypeCourse($typeCourse_id){

        $typeCourse = TypeCourse::where('isDelete', 0)->where('id', $typeCourse_id)->first();
        if($typeCourse)
        {
            $typeCourse->isDelete = 1;
            $typeCourse->save();
            return response()->json(['success' => true, 'message' => 'Ce type de course à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'ce type de course n\'est pas le systéme'], 404);

        }
    }

}
