<?php

namespace App\Http\Controllers\API\TypeMarchands;

use App\Models\TypeMarchand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ControllerTypeMarchand extends Controller
{
    // Récupérer tous les types de marchands
    public function AllTypeMarchand()
    {
        $typeMarchands = TypeMarchand::where('isDelete', 0)->get();

        return response()->json(['success' => true, 'response' => $typeMarchands],200);
    }
    // Ajouter un types de marchands
    public function addTypeMarchand(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required', 'string'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $typeMarchand = TypeMarchand::create([
            'type' => $request->type,
        ]);

        return response()->json(['success' => true, 'response' => $typeMarchand], 201);
    }

    // mettre a jour types de marchands

    public function updateTypeMarchand(Request $request, $typeMarchand_id)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required', 'string'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $typeMarchand = TypeMarchand::where('isDelete', 0)->where('id', $typeMarchand_id)->first();


        $typeMarchand->type = $request->type;
        $typeMarchand->save();



        if($typeMarchand)
        {
            return response()->json(['success' => true, 'response' => $typeMarchand], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Ce type de marchand n\'existe pas dans notre systeme'], 404);

        }

    }

    // supprimer un types de marchands
    public function deleteTypeMarchand($typeMarchand_id){

        $typeMarchand = TypeMarchand::where('isDelete', 0)->where('id', $typeMarchand_id)->first();
        if($typeMarchand)
        {
            $typeMarchand->isDelete = 1;
            $typeMarchand->save();
            return response()->json(['success' => true, 'message' => 'Ce type de marchand à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'ce type de marchand n\'est pas le systéme'], 404);

        }
    }

}
