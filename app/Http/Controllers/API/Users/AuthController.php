<?php

namespace App\Http\Controllers\API\Users;

use App\Models\Note;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /*** BASIC REGISTER **/
    /***
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom' => ['required', 'string', 'max:255'],
            'prenoms' => ['required', 'string', 'max:255'],
            'no_ifu' => ['nullable', 'string', 'max:255'],
            'tel' => ['required', 'string', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'sexe' => ['required', 'string', 'max:10'],
            'password' => ['required', 'string', 'min:8'],
            'piece_utilisateur' => ['nullable', 'json'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $user = User::create([
            'nom' => $request->nom,
            'prenoms' => $request->prenoms,
            'no_ifu' => $request->no_ifu,
            'tel' => $request->tel,
            'email' => $request->email,
            'sexe' => $request->sexe,
            'password' => Hash::make($request->password),
            'piece_utilisateur' => $request->piece_utilisateur,
        ]);

        $userRole = Role::where('name' ,'Utilisateur')->first();
        $user->roles()->attach($userRole);

        $token = $user->createToken('user')->accessToken;
        // dd($token);

        return response()->json(['success' => true, 'response' => ['token' => $token, 'user' => $user]], 201);
    }

    /** BASIC LOGIN */
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken('user')->accessToken;
            return response()->json([
                "success" => true,
                "response" => [
                    "user" => $user,
                    "token" => $token,
                ],
            ], 200);
        }
        else {
            return response()->json(['error' => 'Unauthenticated'], 401);
        }
    }


    public function registerWithGoogle(Request $request)
    {
        // Demander l'autorisation OAuth à Google
        $query = http_build_query([
            'client_id' => env('GOOGLE_CLIENT_ID'),
            'redirect_uri' => env('GOOGLE_REDIRECT_URI'),
            'response_type' => 'code',
            'scope' => 'openid email profile',
            'state' => $request->input('state'),
        ]);
        $url = 'https://accounts.google.com/o/oauth2/v2/auth?' . $query;
        return redirect($url);
    }

    /** GOOGLE AUTH CALLBACK METHOD */
    public function handleGoogleCallback(Request $request)
    {
        // Récupérer le jeton d'accès OAuth de Google à partir du code d'autorisation
        $response = Http::asForm()->post('https://oauth2.googleapis.com/token', [
            'code' => $request->input('code'),
            'client_id' => env('GOOGLE_CLIENT_ID'),
            'client_secret' => env('GOOGLE_CLIENT_SECRET'),
            'redirect_uri' => env('GOOGLE_REDIRECT_URI'),
            'grant_type' => 'authorization_code',
        ]);

        $accessToken = $response->json()['access_token'];

        // Debug : afficher le jeton d'accès pour vérifier qu'il existe bien
        #dd($accessToken);

        // Obtenir les informations de l'utilisateur à partir du jeton d'accès OAuth de Google
        $userInfo = Http::get('https://www.googleapis.com/oauth2/v2/userinfo', [
            'access_token' => $accessToken,
        ])->json();

        // Créer un nouvel utilisateur avec les informations de l'utilisateur récupérées
        $user = User::firstOrCreate([
            'email' => $userInfo['email'],
        ], [
            'nom' => $userInfo['family_name'],
            'prenoms' => $userInfo['given_name'],
            'password' => bcrypt(Str::random(16)),
        ]);

        $userRole = Role::where('name' ,'Utilisateur')->first();
        $user->roles()->attach($userRole);

        // Générer un jeton d'accès pour l'utilisateur
        $token = $user->createToken('Token Name')->accessToken;

        // Retourner le jeton d'accès et les informations de l'utilisateur à l'application cliente
        return response()->json([
            'user' => $user,
            'access_token' => $token,
        ]);
    }

    /****** GOOGLE AUTHENTIFICATION ******/
    public function loginWithGoogle(Request $request)
    {
        // Récupérer l'ID Token de Google envoyé depuis le frontend
        $id_token = $request->input('id_token');

        try {
            // Vérifier l'ID Token avec Google
            $client = new \Google_Client(['client_id' => env('GOOGLE_CLIENT_ID')]);
            $payload = $client->verifyIdToken($id_token);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Invalid ID token'], 400);
        }

        // Récupérer l'adresse e-mail de l'utilisateur depuis le payload de l'ID Token
        $email = $payload['email'];

        // Rechercher l'utilisateur correspondant dans la base de données
        $user = User::where('email', $email)->first();

        if (!$user) {
            // Si l'utilisateur n'existe pas dans la base de données, renvoyer une erreur
            return response()->json(['error' => 'User not found'], 404);
        }

        // Créer un jeton d'accès pour l'utilisateur
        $token = $user->createToken('Token Name')->accessToken;

        // Retourner le jeton d'accès et les informations de l'utilisateur à l'application cliente
        return response()->json([
            'user' => $user,
            'token' => $token,
        ]);
    }

    /** REGISTER WITH FACEBOOK **/
    public function registerWithFacebook(Request $request)
    {
        // Récupérer les informations de l'utilisateur depuis Facebook
        $facebookUser = Socialite::driver('facebook')->userFromToken($request->input('access_token'));

        // Vérifier si l'utilisateur existe déjà dans la base de données
        $user = User::where('facebook_id', $facebookUser->getId())->first();

        if (!$user) {
            // Si l'utilisateur n'existe pas, créer un nouvel utilisateur avec les informations de Facebook
            $user = new User();
            $user->facebook_id = $facebookUser->getId();
            $user->name = $facebookUser->getName();
            $user->email = $facebookUser->getEmail();
            $user->save();
        }

        // Authentifier l'utilisateur
        Auth::login($user);
        // Générer un token d'accès
        $token = $user->createToken('Token name')->accessToken;
        return response()->json(['token' => $token]);
    }

    /** FACEBOOK AUTHENTIFICATION **/
    public function redirectToFacebook()
    {
        // dd('ok');
        return Socialite::driver('facebook')->redirect();
    }

    /** FACEBOOK AUTHENTIFICATION CALLBACK METHOD */
    public function handleFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user();
        die(var_dump($user));
        // Vérifier si l'utilisateur existe dans votre base de données
        $user = User::where('facebook_id', $user->getId())->first();

        // Si l'utilisateur n'existe pas, créer un nouveau utilisateur avec les informations de l'utilisateur Facebook
        if (!$user) {
            $user = new User;
            $user->facebook_id = $user->getId();
            $user->name = $user->getName();
            $user->email = $user->getEmail();
            $user->save();
        }

        // Authentifier l'utilisateur et générer un token d'accès
        $token = $user->createToken('Token Name')->accessToken;

        return response()->json(['token' => $token]);
    }

    // Récupérer tous les users avec les notes
    public function AllUserWithNotes()
    {
        $users = User::where('isDelete', 0)->with('notes')->get();

        return response()->json(['success' => true, 'response' => $users],200);
    }

    // Récupérer un users avec les notes
    public function getUserWithNotes($user_id)
    {
        $user = User::where('isDelete', 0)->with('notes')->find($user_id);

        return response()->json(['success' => true, 'response' => $user],200);
    }

}

