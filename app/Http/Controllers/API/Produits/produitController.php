<?php

namespace App\Http\Controllers\API\Produits;

use App\Models\Produit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class produitController extends Controller
{

    // Récupérer tous les produits
    public function AllProduits()
    {
        $produits = Produit::where('isDelete', 0)->with('notes')->get();

        return response()->json(['success' => true, 'response' => $produits],200);


    }
    // Ajouter un produit
    public function addProduit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom' => ['required', 'string'],
            'description' => ['required'],
            'prix_promo' => ['nullable', 'numeric'],
            'prix' => ['required', 'numeric'],
            'marchand_id' => ['required'],
            'categorie_produit_id' => ['required'],
            'infos_supplementaire' => ['nullable'],
            'images' => ['nullable'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $all_path = $this->uploadOne($request->file('images'));
        $produit = Produit::create([
            'nom' => $request->nom,
            'description' => $request->description,
            'prix_promo' => $request->prix_promo,
            'prix' => $request->prix,
            'marchand_id' => $request->marchand_id,
            'categorie_produit_id' => $request->categorie_produit_id,
            'images' => json_encode($all_path),
            'infos_supplementaire' => $request->infos_supplementaire,
        ]);

        return response()->json(['success' => true, 'response' => $produit], 201);
    }

    // mettre a jour un produit
    public function updateProduit(Request $request, $produit_id)
    {
        $validator = Validator::make($request->all(), [
            'nom' => ['required', 'string'],
            'description' => ['required'],
            'prix_promo' => ['nullable', 'numeric'],
            'prix' => ['required', 'numeric'],
            'marchand_id' => ['required'],
            'categorie_produit_id' => ['required'],
            'infos_supplementaire' => ['nullable'],
            'images' => ['nullable'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $produit = Produit::where('isDelete', 0)->where('id', $produit_id)->first();
        $all_path = $this->uploadOne($request->file('images'));

        $produit->nom = $request->nom;
        $produit->description = $request->description;
        $produit->prix_promo = $request->prix_promo;
        $produit->prix = $request->prix;
        $produit->images = json_encode($all_path);
        $produit->infos_supplementaire = $request->infos_supplementaire;
        $produit->marchand_id = $request->marchand_id;
        $produit->categorie_produit_id = $request->categorie_produit_id;
        $produit->save();



        if($produit)
        {
            return response()->json(['success' => true, 'response' => $produit], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Ce produit n\'existe pas dans notre systeme'], 200);

        }

    }
    // supprimer un produit
    public function deleteProduit($produit_id){

        $produit = Produit::where('isDelete', 0)->where('id', $produit_id)->first();
        if($produit)
        {
            $produit->isDelete = 1;
            $produit->save();
            return response()->json(['success' => true, 'message' => 'Ce produit à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'ce produit n\'est pas le systéme'], 404);

        }
    }
    //  la fonction pour l'enregisterment des images lors de l'ajout du produit
    public function uploadOne($path)
    {

        if (!empty($path)) {
            $array_full = array();
                #$images = $full;
                // return path;
                $imageName = time().uniqid() . '.' . $path->getClientOriginalExtension();
                $path_file = $path->storeAs(
                    'ImageProduit',
                    $imageName,
                    'public'
                );
                array_push($array_full, $path_file);
            return $array_full;

        }
    }

}
