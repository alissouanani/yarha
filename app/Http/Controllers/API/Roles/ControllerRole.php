<?php

namespace App\Http\Controllers\API\Roles;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ControllerRole extends Controller
{
    // Récupérer tous les roles
    public function AllRoles()
    {
        $roles = Role::where('isDelete', 0)->get();

        return response()->json(['success' => true, 'response' => $roles],200);


    }
    // Ajouter un role
    public function addRole(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $role = Role::create([
            'name' => $request->name,
        ]);




        return response()->json(['success' => true, 'response' => $role], 201);
    }
    // mettre a jour un role
    public function updateRole(Request $request, $role_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $role = Role::where('isDelete', 0)->where('id', $role_id)->first();

        $role->name = $request->name;
        $role->save();

        if($role)
        {
            return response()->json(['success' => true, 'response' => $role], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Ce rôle n\'existe pas dans notre systeme'], 404);

        }

    }

    // supprimer un role
    public function deleteRole($role_id){

        $role = Role::where('isDelete', 0)->where('id', $role_id)->first();
        if($role)
        {
            $role->isDelete = 1;
            $role->save();
            return response()->json(['success' => true, 'message' => 'Ce rôle à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'ce rôle n\'est pas le systéme'], 404);

        }
    }

}
