<?php

namespace App\Http\Controllers\API\Livreurs;

use App\Models\Order;
use App\Models\Livreur;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class livreurController extends Controller
{
    // récupérer tous les livreurs
    public function AllLivreurs()
    {
        $livreurs = Livreur::where('isDelete', 0)->with('notes')->get();

        return response()->json(['success' => true, 'response' => $livreurs],200);

    }

    // ajouter un livreur
    public function addLivreur(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => ['required'],
            'matricule_engin' => ['required'],
            'pieces_engin' => ['nullable','json'],
            'note_livreur' => ['nullable'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $livreur = Livreur::create([
            'user_id' => $request->user_id,
            'matricule_engin' => $request->matricule_engin,
            'note_livreur' => $request->note_livreur,
            'pieces_engin' => json_encode($request->pieces_engin),
        ]);

        return response()->json(['success' => true, 'response' => $livreur], 201);
    }
    // mettre a jour un livreur
    public function updateLivreur(Request $request, $livreur_id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => ['required'],
            'matricule_engin' => ['required'],
            'pieces_engin' => ['nullable','json'],
            'note_livreur' => ['nullable'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $livreur = Livreur::where('isDelete', 0)->where('id', $livreur_id)->first();


        $livreur->matricule_engin = $request->matricule_engin;
        $livreur->user_id = $request->user_id;
        $livreur->note_livreur = $request->note_livreur;
        $livreur->pieces_engin = json_encode($request->pieces_engin);
        $livreur->save();



        if($livreur)
        {
            return response()->json(['success' => true, 'response' => $livreur], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Ce livreur n\'existe pas dans notre systeme'], 404);

        }

    }
    // supprimer un livreur
    public function deleteLivreur($livreur_id){

        $livreur = Livreur::where('isDelete', 0)->where('id', $livreur_id)->first();
        if($livreur)
        {
            $livreur->isDelete = 1;
            $livreur->save();
            return response()->json(['success' => true, 'message' => 'Ce livreur à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'ce livreur n\'est pas le systéme'], 404);

        }
    }
    //  fonction permettant a un livreur d'accepter livrer une commande
    public function accepterUneDemande($commande_id, $livreur_id){
        $livreur = Livreur::where('isDelete', 0)->where('id', $livreur_id)->first();
        if($livreur)
        {
            $order = Order::where('isDelete', 0)->where('status', 'en attente')->with('orderItems')->find($commande_id);
            if($order)
            {
                if($order->livreur_id == null)
                {
                    $order->livreur_id = $livreur_id;
                    $order->status = "En cours";
                    $order->save();
                    return response()->json(['success' => true, 'message' => 'Livraison acceptée'], 202);
                }else{
                    return response()->json(['success' => false, 'message' => 'Cette commande à été déja prise'], 404);
                }
            }else{
                return response()->json(['success' => false, 'message' => 'Cette commande n\'est pas le systéme'], 404);
            }

        }else{
            return response()->json(['success' => false, 'message' => 'Ce livreur n\'est pas le systéme'], 404);
        }
    }

    //  fonction permettant de récupérer toutes les commandes en cours de livraison d'un livreur d'accepter

    public function AllDemandeEnCoursUser($livreur_id){
        $order = Order::where('isDelete', 0)->where('status', 'En cours')->where('livreur_id', $livreur_id)->with('orderItems')->get();
        if($order->isNotEmpty())
        {
            return response()->json(['success' => true, 'response' => $order], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Ce livreur n\'a aucune livraison en cours'], 404);
        }
    }
    //  fonction permettant de récupérer toutes les commandes avec statut "livrer" d'un livreur

    public function AllDemandeLivrerUser($livreur_id){
        $order = Order::where('isDelete', 0)->where('status', 'livrer')->where('livreur_id', $livreur_id)->with('orderItems')->get();
        if($order)
        {
            return response()->json(['success' => true, 'response' => $order], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Ce livreur n\'a aucune  livraison terminé'], 404);
        }
    }
    //  fonction permettant a un livreur de changer la statut d'une commande en  "livrer" apres avoir effectué la commande

    public function termineDemandeLivrerUser($commande_id, $livreur_id){
        $livreur = Livreur::where('isDelete', 0)->where('id', $livreur_id)->first();
        if($livreur)
        {
            $order = Order::where('isDelete', 0)->where('status', 'En cours')->where('livreur_id', $livreur_id)->with('orderItems')->find($commande_id);
            if($order)
            {
                $order->status = "livrer";
                $order->save();
                return response()->json(['success' => true, 'message' => "commande terminée"], 200);
            }else{
                return response()->json(['success' => false, 'message' => 'Cette commande déja acceptée ou n\'est pas dans le systeme'], 404);
            }

        }else{
            return response()->json(['success' => false, 'message' => 'Ce livreur n\'est pas dans le systeme'], 404);
        }

    }

}
