<?php

namespace App\Http\Controllers\API\Marchands;

use App\Models\Produit;
use App\Models\Marchand;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class marchandController extends Controller
{
    // récupérer les produits par marchands
    public function produitByMarchands($marchand_id)
    {
        $produits = Produit::where('isDelete', 0)->where('marchand_id', $marchand_id)->with('notes')->get();
        if($produits)
        {
            return response()->json(['success' => true, 'response' => $produits],200);
        }else{
            return response()->json(['success' => false, 'message' => 'Aucun produit n\'a été créer par ce marchand'], 404);
        }
    }
    // récupérer tous les marchands
    public function AllMarchands()
    {
        $marchands = Marchand::where('isDelete', 0)->with('notes')->get();
        return response()->json(['success' => true, 'response' => $marchands],200);
    }
    // Ajouter un marchands
    public function addMarchand(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => ['required'],
            'type_marchand_id' => ['required'],
            'no_ifu' => ['required'],
            'nom' => ['required', 'string'],
            'adresse' => ['required'],
            'contact' => ['required'],
            'email' => ['required'],
            'note_marchand' => ['nullable', 'string'],
            'geo_localisation' => ['nullable', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $marchand = Marchand::create([
            'user_id' => $request->user_id,
            'type_marchand_id' => $request->type_marchand_id,
            'no_ifu' => $request->no_ifu,
            'nom' => $request->nom,
            'adresse' => $request->adresse,
            'contact' => $request->contact,
            'email' => $request->email,
            'note_marchand' => $request->note_marchand,
            'geo_localisation' => json_encode($request->geo_localisation),
        ]);
        return response()->json(['success' => true, 'response' => $marchand], 201);
    }
    // mettre a jour un marchand
    public function updateMarchand(Request $request, $marchand_id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => ['required'],
            'type_marchand_id' => ['required'],
            'no_ifu' => ['required'],
            'nom' => ['required', 'string'],
            'adresse' => ['required'],
            'contact' => ['required'],
            'email' => ['required'],
            'note_marchand' => ['nullable', 'string'],
            'geo_localisation' => ['nullable', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $marchand = Marchand::where('isDelete', 0)->where('id', $marchand_id)->first();

        $marchand->type_marchand_id = $request->type_marchand_id;
        $marchand->user_id = $request->user_id;
        $marchand->no_ifu = $request->no_ifu;
        $marchand->contact = $request->contact;
        $marchand->email = $request->email;
        $marchand->note_marchand = $request->note_marchand;
        $marchand->geo_localisation = json_encode($request->geo_localisation);
        $marchand->save();

        if($marchand)
        {
            return response()->json(['success' => true, 'response' => $marchand], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Ce marchand n\'existe pas dans notre systeme'], 404);
        }
    }
    // supprimer un marchand
    public function deleteMarchand($marchand_id){
        $marchand = Marchand::where('isDelete', 0)->where('id', $marchand_id)->first();
        if($marchand)
        {
            $marchand->isDelete = 1;
            $marchand->save();
            return response()->json(['success' => true, 'message' => 'Ce marchand à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'ce marchand n\'est pas le systéme'], 404);
        }
    }
}
