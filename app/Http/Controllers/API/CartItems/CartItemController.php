<?php

namespace App\Http\Controllers\API\CartItems;

use App\Models\User;
use App\Models\Produit;
use App\Models\CartItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartItemController extends Controller
{
    // Ajouter un produit au panier
    public function addToCart($user_id, $produit_id,$marchand_id)
    {
         // Vérifier si le produit est déjà dans le panier de l'utilisateur
         $cartItem = CartItem::where('user_id', $user_id)
            ->where('marchand_id', $marchand_id)
            ->where('produit_id', $produit_id)
            ->first();


         if ($cartItem) {
             // Mettre à jour la quantité
             $cartItem->quantite += 1;
             $cartItem->save();
            return response()->json(['success' => true, 'message' => 'Ce Produit à été ajouter au panier'], 202);
         } else {

            $dernier_produit = CartItem::where('user_id', $user_id)->where('marchand_id', $marchand_id)->first();
            if($dernier_produit)
            {
                $nouveau_produit = Produit::where('isDelete', 0)->where('id', $produit_id)->first();
                if($dernier_produit->marchand_id == $nouveau_produit->marchand_id)
                {
                    // Ajouter un nouvel élément au panier
                    $cartItem = new CartItem;
                    $cartItem->user_id = $user_id;
                    $cartItem->produit_id = $produit_id;
                    $cartItem->marchand_id = $nouveau_produit->marchand_id;
                    $cartItem->quantite = 1;
                    $cartItem->save();
                    return response()->json(['success' => true, 'message' => 'Ce Produit à été ajouter au panier'], 202);
                }else{
                    return response()->json(['success' => false, 'message' => 'Ce Produit n\'est pas de la même boutique que  les autres produits du panier'], 202);

                }
            }else{
                $nouveau_produit = Produit::where('isDelete', 0)->where('id', $produit_id)->first();

                // Ajouter un nouvel élément au panier
                $cartItem = new CartItem;
                $cartItem->user_id = $user_id;
                $cartItem->produit_id = $produit_id;
                $cartItem->marchand_id = $nouveau_produit->marchand_id;
                $cartItem->quantite = 1;
                $cartItem->save();
                return response()->json(['success' => true, 'message' => 'Ce Produit à été ajouter au panier'], 202);
            }

         }
    }
    // récupérer le panier d'un utilisateur par marchand

    public function getCartItemUser($user_id, $marchand_id){
        $cartItems = CartItem::where('user_id', $user_id)->where('marchand_id', $marchand_id)->with('produit')->get();
        // dd($cartItems);
        foreach($cartItems as $cartItem)
        {
            $total = $cartItem->produit->prix  * $cartItem->quantite;
            $cartItem->total = $total;
            $cartItem->save();
        }

        if($cartItems)
        {
            return response()->json(['success' => true, 'response' => $cartItems], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'Aucun produit dans le panier'], 200);

        }
    }
    // récupérer le panier d'un utilisateur par marchand
    public function getCartItemUserByMarchand($user_id, $marchand_id){
        $cartItems = CartItem::where('user_id', $user_id)->where('marchand_id', $marchand_id)->with('produit')->get();
        // dd($cartItems);
        foreach($cartItems as $cartItem)
        {
            $total = $cartItem->produit->prix  * $cartItem->quantite;
            $cartItem->total = $total;
            $cartItem->save();
        }

        if($cartItems)
        {
            return response()->json(['success' => true, 'response' => $cartItems], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'Aucun produit dans le panier'], 200);

        }
    }
    // supprimer un element du panier
    public function removeFromCart($user_id, $product_id, $marchand_id)
    {
        $user = User::where('isDelete', 0)->where('id', $user_id)->firstOrFail();
        if($user)
        {
            $cartItem = $user->cartItems()->where('produit_id', $product_id)->where('marchand_id', $marchand_id)->firstOrFail();

            if($cartItem)
            {
                $cartItem->delete();
                return response()->json(['success' => true, 'message' => 'Ce produit à été supprimer'], 202);
            }else{
                return response()->json(['success' => false, 'message' => 'Ce produit n\'est pas dans le panier'], 404);
            }
        }else{
            return response()->json(['success' => false, 'message' => 'cet utilisateur ne peut pas supprimer ce produit'], 404);
        }

    }
}
