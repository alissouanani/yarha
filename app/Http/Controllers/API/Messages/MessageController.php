<?php

namespace App\Http\Controllers\API\Messages;

use App\Models\Room;
use App\Models\User;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    // fonction permettant d'envoyer des messages
    public function sendMessage(Request $request)
    {
        $validatedData = $request->validate([
            'send_id' => 'required|exists:users,id',
            'receiver_id' => 'required|exists:users,id',
            'message' => 'required|string',
        ]);

        $sender = User::findOrFail($validatedData['send_id']);
        $receiver = User::findOrFail($validatedData['receiver_id']);
        // verification si send_id et receiver_id sont deja dans un room
        $room = Room::where([
            ['sender_id', $sender->id],
            ['receiver_id', $receiver->id],
        ])
        ->orWhere([
            ['sender_id', $receiver->id],
            ['receiver_id', $sender->id],
        ])->first();

        // s'il y n'a pas un room on cree
        if (!$room) {
            $room = new Room([
                'sender_id' => $sender->id,
                'receiver_id' => $receiver->id,
            ]);
            $room->save();
        }

        $message = new Message([
            'message' => $validatedData['message'],
        ]);
        $message->room()->associate($room);
        $message->save();

        return response()->json(['success' => true, 'message' => 'Message envoyé.'], 200);
    }

    // récupérer tous les rooms ou user_id est un expediteur ou un destinataire avec la relation messages()
    public function getAllRoomUser($user_id){
        $rooms = Room::where('sender_id', $user_id)->orWhere('receiver_id', $user_id)->with('messages')->get();

        if($rooms)
        {
            return response()->json(['success' => true, 'response' => $rooms], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'cet utilisateur n\'a aucun message'], 404);

        }
    }
    // supprimer un message
    public function deleteMessage($user_id, $message_id)
    {
        // verifier si user_id est bien l'auteur du message qui a pour id message_id
        $message = Room::whereHas('messages', function ($query) use ($message_id) {
                $query->where('id', $message_id);
            })
            ->where('sender_id', $user_id)
            ->first()
            ->messages()
            ->where('id', $message_id)
            ->first();


        if($message)
        {
            $message->delete();
            return response()->json(['success' => true, 'message' => 'ce message à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'Vous ne pouvez pas supprimer ce message'], 404);

        }
    }

}
