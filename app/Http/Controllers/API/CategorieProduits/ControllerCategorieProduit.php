<?php

namespace App\Http\Controllers\API\CategorieProduits;

use Illuminate\Http\Request;
use App\Models\CategorieProduit;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ControllerCategorieProduit extends Controller
{
    // récupérer toutes les categories de  produit

    public function AllCategorieProduit()
    {
        $categorieProduits = CategorieProduit::where('isDelete', 0)->get();

        if($categorieProduits)
        {
            return response()->json(['success' => true, 'response' => $categorieProduits],200);
        }else{
            return response()->json(['success' => false, 'message' => 'Aucune catégorie de produit n\'a été créer par ce marchand'], 404);

        }

    }
    // ajouter une categories de  produit

    public function addCategorieProduit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom' => ['required', 'string'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $categorieProduit = CategorieProduit::create([
            'nom' => $request->nom,
        ]);




        return response()->json(['success' => true, 'response' => $categorieProduit], 201);
    }
    // mettre a jour une categories de  produit
    public function updateCategorieProduit(Request $request, $categorieProduit_id)
    {
        $validator = Validator::make($request->all(), [
            'nom' => ['required', 'string'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $categorieProduit = CategorieProduit::where('isDelete', 0)->where('id', $categorieProduit_id)->first();


        $categorieProduit->nom = $request->nom;
        $categorieProduit->save();



        if($categorieProduit)
        {
            return response()->json(['success' => true, 'response' => $categorieProduit], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Cette catégorie de produit n\'existe pas dans notre systeme'], 404);

        }

    }
    //  supprimer une categories de  produit
    public function deleteCategorieProduit($categorieProduit_id){

        $categorieProduit = CategorieProduit::where('isDelete', 0)->where('id', $categorieProduit_id)->first();
        if($categorieProduit)
        {
            $categorieProduit->isDelete = 1;
            $categorieProduit->save();
            return response()->json(['success' => true, 'message' => 'Cette catégorie de produit à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'cette catégorie de produit n\'est pas le systéme'], 404);
        }
    }

}
