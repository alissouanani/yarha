<?php

namespace App\Http\Controllers\API\ParametreLivreur;

use Illuminate\Http\Request;
use App\Models\ParametreLivreur;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ParametreLivreurController extends Controller
{
    // Récupérer tous les parametre de livreurs
    public function AllParametreLivreurs()
    {
        $parametreLivreurs = ParametreLivreur::where('isDelete', 0)->get();
        return response()->json(['success' => true, 'response' => $parametreLivreurs],200);
    }
    // Ajouter un parametre de livreurs

    public function addParametreLivreur(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'commission' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $parametreLivreur = ParametreLivreur::create([
            'commission' => $request->commission,
        ]);

        return response()->json(['success' => true, 'response' => $parametreLivreur], 201);
    }

    // mettre a jour parametre de livreurs

    public function updateParametreLivreur(Request $request, $parametreLivreur_id)
    {
        $validator = Validator::make($request->all(), [
            'commission' => ['required', 'string'],
        ]);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $parametreLivreur = ParametreLivreur::where('isDelete', 0)->where('id', $parametreLivreur_id)->first();

        $parametreLivreur->commission = $request->commission;
        $parametreLivreur->save();

        if($parametreLivreur)
        {
            return response()->json(['success' => true, 'response' => $parametreLivreur], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Cette parametre Livreur n\'existe pas dans notre systeme'], 404);

        }

    }

    // supprimer un parametre de livreurs

    public function deleteParametreLivreur($parametreLivreur_id){

        $parametreLivreur = ParametreLivreur::where('isDelete', 0)->where('id', $parametreLivreur_id)->first();
        if($parametreLivreur)
        {
            $parametreLivreur->isDelete = 1;
            $parametreLivreur->save();
            return response()->json(['success' => true, 'message' => 'Cette parametre Livreur à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'cette parametre Livreur n\'est pas le systéme'], 404);

        }
    }

}
