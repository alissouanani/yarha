<?php

namespace App\Http\Controllers\API\Orders;

use App\Models\User;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    // la fonction permettant de passer une commande
    public function placeOrder($user_id, $marchand_id)
    {
        // Récupérer l'utilisateur qui veux passer la commande
        $user = User::where('isDelete', 0)->where('id', $user_id)->firstOrFail();
        // par la relation cartItems() je récupéere tous les cartItems dont l'marchand_id egal a l'id passé en parametre
        $cartItems = $user->cartItems->where('marchand_id', $marchand_id);
        // calcul le montant total du panier
        $totalAmount = $cartItems->sum('total');
        $produit_marchand = $user->cartItems->where('marchand_id', $marchand_id)->firstOrFail();
        // return $cartItems;
        // enregistrer la commande
        $order = $user->orders()->create([
            'total' => $totalAmount,
            'user_id' => $user_id,
            'marchand_id' => $produit_marchand->marchand_id,
            'type_course_id' => 1,
            'reference' => $this->generateRandomString(10),
            'frais_course' => "45",
        ]);


        // enregistre les produits constituant la commande
        foreach ($cartItems as $cartItem) {
            $produit = $cartItem->produit;
            $order = Order::where('isDelete', 0)->where('user_id', $user->id)->where('marchand_id', $marchand_id)->where('status', "en attente")->latest()->first();
            $order->orderItems()->create([
                'produit_id' => $produit->id,
                'order_id' => $order->id,
                'quantite' => $cartItem->quantite,
                'prix' => $produit->prix,
            ]);
        }


        // supprimer le panier
        $user->cartItems()->where('marchand_id', $marchand_id)->delete();
        return response()->json(['success' => true, 'message' => 'Cette commande à été bien passées'], 202);


    }

    //  fonction permerttant de generer la reference de la commande
    public function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
    // Récupérer toutes les commandes par utilisateur
    public function getOrderUser($user_id){
        $orders = Order::where('isDelete', 0)->where('user_id', $user_id)->with('orderItems')->get();
        if($orders)
        {
            return response()->json(['success' => true, 'response' => $orders], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'Cet utilisateur n\'a aucune commande'], 404);
        }
    }

    // Récupérer toutes les commandes par utilisateur et par marchands

    public function getOrderUserByMarchand($user_id, $marchand_id){
        $orders = Order::where('isDelete', 0)->where('user_id', $user_id)->where('marchand_id', $marchand_id)->with('orderItems')->get();

        if($orders)
        {
            return response()->json(['success' => true, 'response' => $orders], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'Cet utilisateur n\'a aucune commande'], 404);

        }
    }

    //  Récupérer toutes les commandes
    public function getAllOrders(){
        $orders = Order::where('isDelete', 0)->where('status', 'en attente')->with('orderItems')->get();

        if($orders)
        {
            return response()->json(['success' => true, 'response' => $orders], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'Aucune commande en cours'], 404);

        }
    }
}
