<?php

namespace App\Http\Controllers\API\Notes;

use App\Models\Note;
use App\Models\User;
use App\Models\Livreur;
use App\Models\Produit;
use App\Models\Marchand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NoteController extends Controller
{
    // fonction permettant a un user de noter un produit
    public function noteUserProduit(Request $request, $user_id, $produit_id)
    {
        $validatedData = $request->validate([
            'note' => 'required',
        ]);

        $user = User::where('isDelete', 0)->find($user_id);

        if($user)
        {
            $produit = Produit::where('isDelete', 0)->find($produit_id);

            if($produit){

                $note = new Note();
                $note->note = $validatedData['note'];
                $note->user_id = $user_id;
                $note->produit_id = $produit_id;

                $produit->notes()->save($note, ['notable_type' => "App\Models\Produit", 'notable_id' => $produit_id]);
                $produit = Produit::where('isDelete', 0)->with('notes')->find($produit_id);
                $note = $produit->notes->sum('note') / $produit->notes->count();
                $produit->note_produit = $note;
                $produit->save();

                return response()->json(['success' => true, 'message' => 'ce produit à été noté'], 202);
            }else{
                return response()->json(['success' => false, 'message' => 'ce produit n\'existe pas dans notre systeme'], 404);
            }

        }else{
            return response()->json(['success' => false, 'message' => 'cet utilisateur n\'existe pas dans notre systeme'], 404);
        }
    }

    // fonction permettant a un user de noter un livreur

    public function noteUserLivreur(Request $request, $user_id, $livreur_id)
    {
        $validatedData = $request->validate([
            'note' => 'required',
        ]);

        $user = User::where('isDelete', 0)->find($user_id);

        if($user)
        {
            $livreur = Livreur::where('isDelete', 0)->find($livreur_id);

            if($livreur){

                $note = new Note();
                $note->note = $validatedData['note'];
                $note->user_id = $user_id;
                $note->livreur_id = $livreur_id;

                $livreur->notes()->save($note, ['notable_type' => Livreur::class, 'notable_id' => $livreur_id]);
                $livreur = Livreur::where('isDelete', 0)->with('notes')->find($livreur_id);
                $note = $livreur->notes->sum('note') / $livreur->notes->count();
                $livreur->note_livreur = $note;
                $livreur->save();

                return response()->json(['success' => true, 'message' => 'ce livreur à été noté'], 202);
            }else{
                return response()->json(['success' => false, 'message' => 'ce livreur n\'existe pas dans notre systeme'], 404);
            }

        }else{
            return response()->json(['success' => false, 'message' => 'cet utilisateur n\'existe pas dans notre systeme'], 404);
        }
    }

    // fonction permettant a un user de noter un marchand


    public function noteUserMarchand(Request $request, $user_id, $marchand_id)
    {
        $validatedData = $request->validate([
            'note' => 'required',
        ]);

        $user = User::where('isDelete', 0)->find($user_id);

        if($user)
        {
            $marchand = Marchand::where('isDelete', 0)->find($marchand_id);

            if($marchand){

                $note = new Note();
                $note->note = $validatedData['note'];
                $note->user_id = $user_id;
                $note->marchand_id = $marchand_id;

                $marchand->notes()->save($note, ['notable_type' => "App\Models\Marchand", 'notable_id' => $marchand_id]);
                $marchand = Marchand::where('isDelete', 0)->with('notes')->find($marchand_id);
                $note = $marchand->notes->sum('note') / $marchand->notes->count();
                $marchand->note_marchand = $note;
                $marchand->save();

                return response()->json(['success' => true, 'message' => 'ce Marchand à été noté'], 202);
            }else{
                return response()->json(['success' => false, 'message' => 'ce Marchand n\'existe pas dans notre systeme'], 404);
            }

        }else{
            return response()->json(['success' => false, 'message' => 'cet utilisateur n\'existe pas dans notre systeme'], 404);
        }
    }
    // fonction permettant a un livreur de noter un user

    public function notelivreurUser(Request $request, $livreur_id, $user_id)
    {
        $validatedData = $request->validate([
            'note' => 'required',
        ]);

        $livreur = Livreur::where('isDelete', 0)->find($livreur_id);

        // return $livreur;

        if($livreur)
        {
            $user = User::where('isDelete', 0)->find($user_id);

            if($user){

                $note = new Note();
                $note->note = $validatedData['note'];
                $note->user_id = $user_id;
                $note->livreur_id = $livreur_id;

                $user->notes()->save($note, ['notable_type' => User::class, 'notable_id' => $user_id]);
                $user = User::where('isDelete', 0)->with('notes')->find($user_id);
                $note = $user->notes->sum('note') / $user->notes->count();
                $user->note = $note;
                $user->save();

                return response()->json(['success' => true, 'message' => 'Cet utilisateur à été noté'], 202);
            }else{
                return response()->json(['success' => false, 'message' => 'Cet utilisateur n\'existe pas dans notre systeme'], 404);
            }

        }else{
            return response()->json(['success' => false, 'message' => 'ce livreur n\'existe pas dans notre systeme'], 404);
        }
    }
}
