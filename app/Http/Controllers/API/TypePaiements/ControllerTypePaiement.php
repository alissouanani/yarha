<?php

namespace App\Http\Controllers\API\TypePaiements;

use App\Models\TypePaiement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ControllerTypePaiement extends Controller
{
    // Récupérer tous les type de paiements
    public function AllTypePaiement()
    {
        $typePaiements = TypePaiement::where('isDelete', 0)->get();

        return response()->json(['success' => true, 'response' => $typePaiements],200);
    }
    // Ajouter un type de paiements
    public function addTypePaiement(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $typePaiement = TypePaiement::create([
            'type' => $request->type,
        ]);

        return response()->json(['success' => true, 'response' => $typePaiement], 201);
    }

    // mettre a jout type de paiements

    public function updateTypePaiement(Request $request, $typePaiement_id)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $typePaiement = TypePaiement::where('isDelete', 0)->where('id', $typePaiement_id)->first();

        $typePaiement->type = $request->type;
        $typePaiement->save();

        if($typePaiement)
        {
            return response()->json(['success' => true, 'response' => $typePaiement], 200);
        }else{
            return response()->json(['success' => false, 'message' => 'Ce type de course n\'existe pas dans notre systeme'], 404);
        }

    }

    // supprimer un type de paiements
    public function deleteTypePaiement($typePaiement_id){

        $typePaiement = TypePaiement::where('isDelete', 0)->where('id', $typePaiement_id)->first();
        if($typePaiement)
        {
            $typePaiement->isDelete = 1;
            $typePaiement->save();
            return response()->json(['success' => true, 'message' => 'Ce type de course à été bien supprimer'], 202);
        }else{
            return response()->json(['success' => false, 'message' => 'ce type de course n\'est pas le systéme'], 404);

        }
    }

}
