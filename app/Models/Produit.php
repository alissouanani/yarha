<?php

namespace App\Models;

use App\Models\Note;
use App\Models\CartItem;
use App\Models\Marchand;
use App\Models\ProduitCommande;
use App\Models\CategorieProduit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Produit extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function notes()
    {
        return $this->morphMany(Note::class, 'notable');
    }

    public function marchand()
    {
        return $this->belongsTo(Marchand::class);
    }

    public function categorieProduit()
    {
        return $this->belongsTo(CategorieProduit::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function cartItems()
    {
        return $this->hasMany(CartItem::class);
    }

}
