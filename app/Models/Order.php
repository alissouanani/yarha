<?php

namespace App\Models;

use App\Models\User;
use App\Models\Livreur;
use App\Models\Marchand;
use App\Models\OrderItem;
use App\Models\TypeCourse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function livreur()
    {
        return $this->belongsTo(Livreur::class);
    }

    public function typeCourse()
    {
        return $this->belongsTo(TypeCourse::class);
    }

    public function orderMarchands()
    {
        return $this->hasMany(Marchand::class);
    }
}
