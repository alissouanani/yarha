<?php

namespace App\Models;

use App\Models\Note;
use App\Models\User;
use App\Models\ParametreLivreur;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Livreur extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function notes()
    {
        return $this->morphMany(Note::class, 'notable');
    }

    public function userNotes()
    {
        return $this->hasMany(Note::class, 'user_id');
    }

    public function parametreLivreur()
    {
        return $this->belongsTo(ParametreLivreur::class, 'parametre_livreur_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
