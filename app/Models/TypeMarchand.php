<?php

namespace App\Models;

use App\Models\Marchand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TypeMarchand extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function marchands()
    {
        return $this->hasMany(Marchand::class);
    }
}
