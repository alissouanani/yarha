<?php

namespace App\Models;

use App\Models\Note;
use App\Models\User;
use App\Models\TypeMarchand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Marchand extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function notes()
    {
        return $this->morphMany(Note::class, 'notable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function typeMarchand()
    {
        return $this->belongsTo(TypeMarchand::class);
    }
}
