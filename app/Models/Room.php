<?php

namespace App\Models;

use App\Models\User;
use App\Models\Message;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Room extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function sentMessages()
    {
        return $this->hasMany(User::class, 'sender_id');
    }

    public function receivedMessages()
    {
        return $this->hasMany(User::class, 'receiver_id');
    }
}
