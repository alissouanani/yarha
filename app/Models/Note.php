<?php

namespace App\Models;

use App\Models\User;
use App\Models\Livreur;
use App\Models\Produit;
use App\Models\Marchand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Note extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function notable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function produit()
    {
        return $this->belongsTo(Produit::class);
    }

    public function marchand()
    {
        return $this->belongsTo(Marchand::class);
    }

    public function livreur()
    {
        return $this->belongsTo(Livreur::class);
    }
}
