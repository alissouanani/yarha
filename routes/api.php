<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Notes\NoteController;
use App\Http\Controllers\API\Roles\ControllerRole;
use App\Http\Controllers\API\Users\AuthController;
use App\Http\Controllers\API\Orders\OrderController;
use App\Http\Controllers\API\Livreurs\livreurController;
use App\Http\Controllers\API\Messages\MessageController;
use App\Http\Controllers\API\Produits\produitController;
use App\Http\Controllers\API\CartItems\CartItemController;
use App\Http\Controllers\API\Marchands\marchandController;
use App\Http\Controllers\API\TypeCoures\ControllerTypeCourse;
use App\Http\Controllers\API\Permissions\ControllerPermission;
use App\Http\Controllers\API\TypeMarchands\ControllerTypeMarchand;
use App\Http\Controllers\API\TypePaiements\ControllerTypePaiement;
use App\Http\Controllers\API\ParametreLivreur\ParametreLivreurController;
use App\Http\Controllers\API\CategorieProduits\ControllerCategorieProduit;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**ROUTES WITHOUT AUTH ***/
Route::post('/register', [AuthController::class,'register']);
Route::post('/login', [AuthController::class,'login'])->name('login');
Route::get('/auth/google', [AuthController::class,'registerWithGoogle']);
Route::post('/register/google', [AuthController::class,'registerWithGoogle']);
Route::post('/login/google', [AuthController::class,'loginWithGoogle']);
Route::get('/callback/google', [AuthController::class,'handleGoogleCallback']);

Route::get('/auth/facebook', [AuthController::class,'redirectToFacebook']);
Route::get('/callback/facebook', [AuthController::class,'handleFacebookCallback']);

Route::get('/users', [AuthController::class,'AllUserWithNotes'])->middleware('auth:api');
Route::get('/users/{user_id}', [AuthController::class,'getUserWithNotes'])->middleware('auth:api');


// toutes les route des categories produits
Route::get('/categorie-produits', [ControllerCategorieProduit::class,'AllCategorieProduit']);
Route::post('/categorie-produit', [ControllerCategorieProduit::class,'addCategorieProduit'])->middleware('auth:api');
Route::put('/categorie-produit/{id}', [ControllerCategorieProduit::class,'updateCategorieProduit'])->middleware('auth:api');
Route::delete('/categorie-produit/{id}', [ControllerCategorieProduit::class,'deleteCategorieProduit'])->middleware('auth:api');

// toutes les route des permissions
Route::get('/permissions', [ControllerPermission::class,'AllPermissions'])->middleware('auth:api');
Route::post('/permission', [ControllerPermission::class,'addPermission'])->middleware('auth:api');
Route::put('/permission/{id}', [ControllerPermission::class,'updatePermission'])->middleware('auth:api');
Route::delete('/permission/{id}', [ControllerPermission::class,'deletePermission'])->middleware('auth:api');

// toutes les route des marchands
Route::get('/produit-by-marchands/{id}', [marchandController::class,'produitByMarchands']);
Route::get('/marchands', [marchandController::class,'AllMarchands'])->middleware('auth:api');
Route::post('/marchand', [marchandController::class,'addMarchand'])->middleware('auth:api');
Route::put('/marchand/{id}', [marchandController::class,'updateMarchand'])->middleware('auth:api');
Route::delete('/marchand/{id}', [marchandController::class,'deleteMarchand'])->middleware('auth:api');

// toutes les route des livreurs
Route::get('/livreurs', [livreurController::class,'AllLivreurs'])->middleware('auth:api');
Route::put('/accepte/commandes/{commande_id}/livreurs/{livreur_id}', [livreurController::class,'accepterUneDemande'])->middleware('auth:api');
Route::put('/livraison-terminer/commandes/{commande_id}/livreurs/{livreur_id}', [livreurController::class,'termineDemandeLivrerUser'])->middleware('auth:api');
Route::get('/commandes-en-cours/livreurs/{livreur_id}', [livreurController::class,'AllDemandeEnCoursUser'])->middleware('auth:api');
Route::get('/commandes-livreur/livreurs/{livreur_id}', [livreurController::class,'AllDemandeLivrerUser'])->middleware('auth:api');
Route::post('/livreur', [livreurController::class,'addLivreur'])->middleware('auth:api');
Route::put('/livreur/{id}', [livreurController::class,'updateLivreur'])->middleware('auth:api');
Route::delete('/livreur/{id}', [livreurController::class,'deleteLivreur'])->middleware('auth:api');

// toutes les route des parametre livreurs
Route::get('/parametre-livreurs', [ParametreLivreurController::class,'AllParametreLivreurs'])->middleware('auth:api');
Route::post('/parametre-livreur', [ParametreLivreurController::class,'addParametreLivreur'])->middleware('auth:api');
Route::put('/parametre-livreur/{id}', [ParametreLivreurController::class,'updateParametreLivreur'])->middleware('auth:api');
Route::delete('/parametre-livreur/{id}', [ParametreLivreurController::class,'deleteParametreLivreur'])->middleware('auth:api');

// toutes les route des rôles
Route::get('/roles', [ControllerRole::class,'AllRoles'])->middleware('auth:api');
Route::post('/role', [ControllerRole::class,'addRole'])->middleware('auth:api');
Route::put('/role/{id}', [ControllerRole::class,'updateRole'])->middleware('auth:api');
Route::delete('/role/{id}', [ControllerRole::class,'deleteRole'])->middleware('auth:api');

// toutes les route des types de courses
Route::get('/type-courses', [ControllerTypeCourse::class,'AllTypeCourse'])->middleware('auth:api');
Route::post('/type-course', [ControllerTypeCourse::class,'addTypeCourse'])->middleware('auth:api');
Route::put('/type-course/{id}', [ControllerTypeCourse::class,'updateTypeCourse'])->middleware('auth:api');
Route::delete('/type-course/{id}', [ControllerTypeCourse::class,'deleteTypeCourse'])->middleware('auth:api');

// toutes les route des types marchands
Route::get('/type-marchands', [ControllerTypeMarchand::class,'AllTypeMarchand'])->middleware('auth:api');
Route::post('/type-marchand', [ControllerTypeMarchand::class,'addTypeMarchand'])->middleware('auth:api');
Route::put('/type-marchand/{id}', [ControllerTypeMarchand::class,'updateTypeMarchand'])->middleware('auth:api');
Route::delete('/type-marchand/{id}', [ControllerTypeMarchand::class,'deleteTypeMarchand'])->middleware('auth:api');

// toutes les route des types de paiements
Route::get('/type-paiements', [ControllerTypePaiement::class,'AllTypePaiement'])->middleware('auth:api');
Route::post('/type-paiement', [ControllerTypePaiement::class,'addTypePaiement'])->middleware('auth:api');
Route::put('/type-paiement/{id}', [ControllerTypePaiement::class,'updateTypePaiement'])->middleware('auth:api');
Route::delete('/type-paiement/{id}', [ControllerTypePaiement::class,'deleteTypePaiement'])->middleware('auth:api');

// toutes les route des produits
Route::get('/produits', [produitController::class,'AllProduits']);
Route::post('/produit', [produitController::class,'addProduit'])->middleware('auth:api');
Route::put('/produit/{id}', [produitController::class,'updateProduit'])->middleware('auth:api');
Route::delete('/produit/{id}', [produitController::class,'deleteProduit'])->middleware('auth:api');

// toutes les route des carts
Route::get('/users/{user_id}/marchands/{marchand_id}/cart', [CartItemController::class,'getCartItemUserByMarchand'])->middleware('auth:api');
Route::post('/users/{user_id}/produits/{produit_id}/marchands/{marchand_id}/cart', [CartItemController::class,'addToCart'])->middleware('auth:api');
Route::delete('users/{user_id}/produits/{produit_id}/marchands/{marchand_id}/cart', [CartItemController::class,'removeFromCart'])->middleware('auth:api');

// toutes les route des orders
Route::post('/users/{user_id}/marchands/{marchand_id}/commande', [OrderController::class,'placeOrder'])->middleware('auth:api');
Route::get('/users/{id}/commande', [OrderController::class,'getOrderUser'])->middleware('auth:api');
Route::get('/commandes', [OrderController::class,'getAllOrders'])->middleware('auth:api');
Route::get('/users/{user_id}/marchands/{marchand_id}/commande', [OrderController::class,'getCartItemUserByMarchand'])->middleware('auth:api');

// toutes les route des messages
Route::post('/add-message', [MessageController::class,'sendMessage'])->middleware('auth:api');
Route::get('/users/{user_id}/rooms', [MessageController::class, 'getAllRoomUser'])->middleware('auth:api');
Route::delete('/delete/users/{user_id}/messages/{message_id}', [MessageController::class, 'deleteMessage'])->middleware('auth:api');

// toutes les route des notes
Route::post('/note-produits/users/{user_id}/produits/{produit_id}', [NoteController::class,'noteUserProduit'])->middleware('auth:api');
Route::post('/note-livreurs/users/{user_id}/livreurs/{livreur_id}', [NoteController::class,'noteUserLivreur'])->middleware('auth:api');
Route::post('/note-marchands/users/{user_id}/marchands/{marchand_id}', [NoteController::class,'noteUserMarchand'])->middleware('auth:api');
Route::post('/note-users/livreurs/{livreur_id}/users/{user_id}', [NoteController::class,'notelivreurUser'])->middleware('auth:api');




